## SOC Committee Members

- Jean-Baptiste Bayle
- Tjarda Boekholt
- Paul Brook
- Debatri Chattopadhyay
- Gareth Cabourn Davies
- Alexandre Sébastien Göttel
- Charlie Hoy (Chair)
- Ali James
- Hannah Middleton
- Isobel Romero-Shaw

## Request

We are requesting a "key session" of six 90 minute slots. Our session must
fit within the "Origins" theme (interpreted broadly)

## Proposal

Our title and abstract can be found [here](https://gitlab.com/hoyc1/nam-2023/-/blob/main/proposal.pdf).
